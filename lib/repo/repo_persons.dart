// ignore_for_file: avoid_print

import 'package:lesson2/dto/product_data.dart';
import 'package:lesson2/generated/l10n.dart';
import 'package:lesson2/repo/api.dart';

class RepoPersons {
  RepoPersons({required this.api});

  final Api api;

  Future<ResultRepoPersons> filterByName(String name) async {
    try {
      final result = await api.dio.get(
        '/products',
        // queryParameters: {
        //   "name": name,
        // },
      );
      print('result $result');

      final List productsListJson = result.data ?? [];
      final productsList = productsListJson
          .map(
            (item) => ProductData.fromJson(item),
          )
          .toList();
      return ResultRepoPersons(productsList: productsList);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoPersons(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoPersons {
  ResultRepoPersons({
    this.errorMessage,
    this.productsList,
  });

  final String? errorMessage;
  final List<ProductData>? productsList;
}
