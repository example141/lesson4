import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lesson2/bloc/events.dart';
import 'package:lesson2/dto/product_data.dart';

import 'package:lesson2/repo/repo_persons.dart';

part 'states.dart';

class BlocPersons extends Bloc<EventBlocPersons, StateBlocPersons> {
  BlocPersons({
    required this.repo,
  }) : super(StatePersonsInitial()) {
    on<EventPersonsFilterByName>(
      (event, emit) async {
        emit(StatePersonsLoading());
        final result = await repo.filterByName(event.name);
        if (result.errorMessage != null) {
          emit(
            StatePersonsError(result.errorMessage!),
          );
          return;
        }
        emit(
          StatePersonsData(data: result.productsList!),
        );
      },
    );
  }

  final RepoPersons repo;
}
