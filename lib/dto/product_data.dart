import 'package:lesson2/dto/rate.dart';

class ProductData {
  const ProductData({
    this.id,
    this.title,
    this.price,
    this.description,
    this.category,
    this.image,
    this.rating,
  });
  final int? id;
  final String? title;
  final double? price;

  final String? description;
  final String? category;
  final String? image;
  final Rate? rating;
  factory ProductData.fromJson(Map<String, dynamic>? json) {
    if (json == null) return const ProductData();
    return ProductData(
      id: json["id"],
      title: json["title"],
      price: (json['price'] as num?)?.toDouble(),
      description: json["description"],
      category: json["category"],
      image: json["image"],
      rating: json["rating"] == null ? null : Rate.fromJson(json["rating"]),
    );
  }
}
