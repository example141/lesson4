class Rate {
  const Rate({
    this.rate,
    this.count,
  });
  final double? rate;
  final int? count;
  factory Rate.fromJson(Map<String, dynamic>? json) {
    if (json == null) return const Rate();
    return Rate(
      rate: (json['rate'] as num?)?.toDouble(),
      count: json["count"],
    );
  }
}
