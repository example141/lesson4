abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

  final String noAvatar = 'lib/assets/images/bitmap/no_avatar.png';
  final String logo = 'lib/assets/images/bitmap/logo.png';
  final String rick = 'lib/assets/images/bitmap/rick.png';
  final String morty = 'lib/assets/images/bitmap/morty.png';
  final String background = 'lib/assets/images/bitmap/background.png';
}

class _Svg {
  const _Svg();
  final String logo = 'lib/assets/images/svg/logo.svg';
  final String account = 'lib/assets/images/svg/account.svg';
  final String password = 'lib/assets/images/svg/password.svg';
  final String persons = 'lib/assets/images/svg/persons.svg';
}
