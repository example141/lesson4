import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lesson2/constants/app_colors.dart';
import 'package:lesson2/ui/login_screen/login_screen.dart';
import 'package:lesson2/ui/products_screen/widgets/init_widget.dart';
import 'package:lesson2/ui/settings_screen.dart';
import 'package:lesson2/ui/splash_screen.dart';
import 'generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: AppColors.splashBackground,
      statusBarIconBrightness: Brightness.light,
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InitWidget(
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate
        ],
        supportedLocales: S.delegate.supportedLocales,
        routes: {
          '/': (context) => const SplashScreen(),
          '/login_screen': (context) => const LoginScreen(),
          '/settings_screen': (context) => const SettingsScreen(),
        },
        locale: const Locale('ru', 'RU'),
      ),
    );
  }
}
