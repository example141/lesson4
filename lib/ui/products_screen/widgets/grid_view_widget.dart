import 'package:flutter/material.dart';
import 'package:lesson2/dto/product_data.dart';
import 'package:lesson2/ui/products_screen/widgets/product_grid_tile.dart';

class GridViewWidget extends StatelessWidget {
  const GridViewWidget({
    Key? key,
    required this.productsList,
  }) : super(key: key);

  final List<ProductData> productsList;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      mainAxisSpacing: 20.0,
      crossAxisSpacing: 8.0,
      childAspectRatio: 0.8,
      crossAxisCount: 2,
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      children: productsList.map((product) {
        return InkWell(
          child: ProductGridTile(product),
          onTap: () {},
        );
      }).toList(),
    );
  }
}
