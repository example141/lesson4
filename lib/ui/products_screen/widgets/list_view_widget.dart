import 'package:flutter/material.dart';
import 'package:lesson2/dto/product_data.dart';
import 'package:lesson2/ui/products_screen/widgets/product_list_tile.dart';

class ListViewWidget extends StatelessWidget {
  const ListViewWidget({
    Key? key,
    required this.productsList,
  }) : super(key: key);

  final List<ProductData> productsList;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      itemCount: productsList.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: ProductListTile(productsList[index]),
          onTap: () {},
        );
      },
      separatorBuilder: (context, _) => const SizedBox(height: 26.0),
    );
  }
}
