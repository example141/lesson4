import 'package:flutter/material.dart';
import 'package:lesson2/constants/app_colors.dart';
import 'package:lesson2/constants/app_styles.dart';
import 'package:lesson2/dto/product_data.dart';
import 'package:lesson2/generated/l10n.dart';
import 'package:lesson2/ui/products_screen/widgets/user_avatar.dart';

class ProductGridTile extends StatelessWidget {
  const ProductGridTile(this.product, {Key? key}) : super(key: key);

  final ProductData product;

  // Color _statusColor(String? status) {
  //   if (status == 'Dead') return Colors.red;
  //   if (status == 'Alive') return const Color(0xff00c48c);
  //   return Colors.grey;
  // }

  // String _statusLabel(String? status) {
  //   if (status == 'Dead') return S.current.dead;
  //   if (status == 'Alive') return S.current.alive;
  //   return S.current.noData;
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        UserAvatar(
          product.image,
          radius: 60.0,
          margin: const EdgeInsets.only(bottom: 20.0),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      //(product.status).toUpperCase(),
                      'Pr',
                      style: AppStyles.s10w500.copyWith(
                        letterSpacing: 1.5,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      product.title ?? S.of(context).noData,
                      textAlign: TextAlign.center,
                      style: AppStyles.s16w500,
                    ),
                  ),
                ],
              ),
              Row(
                children: const [
                  Expanded(
                    child: Text(
                      //'${product.species ?? S.of(context).noData}, ${product.gender ?? S.of(context).noData}',
                      'Text',
                      style: TextStyle(
                        color: AppColors.neutral2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
